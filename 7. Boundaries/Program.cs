﻿using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Newtonsoft.Json.Linq
{
    class Geocoding
    {
        public static string readLocation()
        {
            Console.WriteLine("Please enter the name of the place for which coordinates is required: \n");
            string name =Console.ReadLine();
            while(string.IsNullOrEmpty(name))
            {
                Console.WriteLine("Location name cannot be Empty. Try again !\n");
                name = Console.ReadLine();
            }
            return name;
        }
        public static string apiRequest(string address)
        {
            WebClient webClient = new WebClient();
            try
            {
                return webClient.DownloadString("https://api.opencagedata.com/geocode/v1/json?q=" + address + "&key=e3cdfe33367b4d7790c367d875cf345d");    
            }
           
            catch(Exception e)
            {
                return "\n ERROR OCCURED : Request failed \n" + e.Message;
            }
            throw new Exception();    
        }
        static void Main(string[] args)
        {     
            
            string address =  readLocation();
            dynamic apiResponse= JsonConvert.DeserializeObject(apiRequest(address));
            try
            {

                GeoLocation geoLocation= new GeoLocation();
                geoLocation.lattitude=apiResponse["results"][0]["geometry"]["lat"];
                geoLocation.longitude=apiResponse["results"][0]["geometry"]["lng"];

                geoLocation.PrintCoordinates();        
            
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Invalid Input. The entered text is not a place name\n");
            }
            catch(Exception e)
            {
                Console.WriteLine("Your input is invalid \n");
            }
        }
    }
    class GeoLocation
    {
        public double longitude;
        public double lattitude;
        public void PrintCoordinates()
        { 
            Console.WriteLine("Latitude :" + this.lattitude);
            Console.WriteLine("Longitude :" + this.longitude);
        }
    }  
}

