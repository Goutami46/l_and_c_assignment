﻿
//No spacing between lines of code and semicolon
using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

//Enough horizontal spacing to differentiate between libraries and start of code
namespace Newtonsoft.Json.Linq
{
    class Global
    {
        //Consistent horizontal spacing between datatypes and variables
	    public static string blogName, url;
        public static dynamic Obj;
        public static int startpoint, endpoint;
    }
    //Eliminating space between two words in class name and Camel casing them/using underscores consistently
    class PostRequest
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reading input from the user:\n");
            readInput();
            Console.WriteLine("storing url:\n");
            serverCall();
            Console.WriteLine("Fetching response:\n");
            processResponse();
            Console.WriteLine("printing blog information:\n");
            printInfo();

	        //Vertical line spacing to separate two different methods
            void readInput()
            {
                Console.WriteLine("Enter the Tumblr blog name:");
                Global.blogName = Console.ReadLine();
                //Enter the range of posts
                Console.WriteLine("enter the start point (between 1 to 50):");
                Global.startpoint = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("enter the end point (between 1 to 50):");
                Global.endpoint = Convert.ToInt32(Console.ReadLine());
            }

            void serverCall()
            {   
		//Consistent Horizontal spacing for expressions
                if ((Global.startpoint >= 1 && Global.endpoint <= 50) && (Global.endpoint > Global.startpoint))
                {

                    Global.url = "https://" + Global.blogName + ".tumblr.com/api/read/json?type=photo&num=" + Global.endpoint + "&start=" + Global.startpoint;
                }

                else
                {
                    Console.WriteLine("The entered input is not valid \n");
                    System.Environment.Exit(0);
                }

            }

            void processResponse()
            {
                WebClient client = new WebClient();
                string ServerResponse = client.DownloadString(Global.url);
                //Fetching response length
                while (ServerResponse.StartsWith("var tumblr_api_read = "))
                {
                    ServerResponse = ServerResponse.Substring("var tumblr_api_read = ".Length);
                }
                ServerResponse = ServerResponse.Replace(";", "");
                Console.WriteLine(ServerResponse);
                //Converting JSON to dynamic object 
                Global.Obj = JsonConvert.DeserializeObject<dynamic>(ServerResponse);
                Console.WriteLine(Global.Obj);
            }

            void printInfo()
            {
		        //Consistent vertical spacing between lines within blocks like try and catch
                try
                {
                    string title = Global.Obj["tumblelog"]["title"].ToString();
                    string name = Global.Obj["tumblelog"]["name"].ToString();
                    string description = Global.Obj["tumblelog"]["description"].ToString();
                    string numOfPosts = Global.Obj["posts-total"].ToString();
                    Console.WriteLine("title: " + title);
                    Console.WriteLine("name: " + name);
                    Console.WriteLine("description: " + description);
                    Console.WriteLine("numOfPosts: " + numOfPosts);

		            //One line space between code performing different actions
                    //Creating JSON object 
                    JArray images = (JArray)Global.Obj["posts"];
                    //printing number of posts in given range
                    Console.WriteLine(images.Count);
                    //Printing urls of the posts and fetching posts with dimension 1280
                    for (int i = 0; i < images.Count; i++)
                    {
                        Console.WriteLine(Global.Obj["posts"][i]["photo-url-1280"].ToString());
                    }
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                }
		        //Continous indentation to close brackets vertically linear to the opening brackets
            }
        }

    }
}


