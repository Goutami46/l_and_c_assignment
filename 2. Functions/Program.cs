﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class GuessCorrectNum
    {

	    static bool ValidateNumberRange(int guessedNumber)
	    {
		    if((guessedNumber>= 1) && (guessedNumber <= 100))
		    {
		        return true;
            }
		
	    	else
	    	{
		    	return false;
	        }
	     }
	
         public static void Main(String[] args) 
         {
                
                int guessedNumber = 0;
                bool guess=false;
                Random rand = new Random();
                int randomNum = rand.Next(1,100);
                Console.WriteLine(randomNum);
                Console.WriteLine("Guess a number between 1 and 100");
                guessedNumber=Convert.ToInt32(Console.ReadLine());
    		    int counter = 0;
    		    
        		while (!guess) 
        		{
        			if (!ValidateNumberRange(guessedNumber)) 
        			{
        				Console.WriteLine("I won't count this number,guess again between 1 to 100");
        				guessedNumber = Convert.ToInt32(Console.ReadLine());
        				
        			} 
        			else 
        			{
        				 counter+= 1;
        				
        			}
        			
        		
        
        			if (guessedNumber < randomNum) 
        			{
        				Console.WriteLine("Too low, Guess again");
                		guessedNumber=Convert.ToInt32(Console.ReadLine());

        			} 
        			else if (guessedNumber > randomNum)  
        			{
        				Console.WriteLine("Too high,guess again");
                		guessedNumber=Convert.ToInt32(Console.ReadLine());
        	
        			} 
        			else 
        			{
        				Console.WriteLine("you guessed it in " + counter + " attempts");
        				guess = true;
        			}
					
        		}
				
        	}
        
        }