﻿using System;

namespace Authentication
{    
    public class Authenticate
    {
        public class Customer 
        {

            private Wallet customerWallet;
            public float payAmount(float payment) 
            {
                customerWallet=new Wallet(100.0f);
                float amountToPay=0.0f;  
                if (customerWallet.getTotalMoney()>= payment) 
                {
                    customerWallet.subtractMoney(payment);
                    amountToPay=payment;
                }
                return amountToPay;   
            }
        }

        public class Wallet 
       { 
            private float value=0.0f;
            public Wallet(float initialAmount)
            {
                value=initialAmount;
            }
            public float getTotalMoney()
            {
                return value;
            }
            public void setTotalMoney(float newValue)
            {
                value=newValue;
            }
            public void addMoney(float deposit) 
            { 
                value += deposit; 
            }    
            public void subtractMoney(float debit) 
            { 
                value -= debit; 
            } 
        }
        
        
    public class MainPayment
    {
        public static void Main(String[] args)        
        {
            string sOTP= "";
            bool validCredential;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\n Please make a choice:");
                Console.WriteLine("1. Authenticate using User Id and password");
                Console.WriteLine("2. Authenticate using OTP on registered mobile number"); 
                 Console.WriteLine("3. Exit");

                Console.WriteLine("Select your choice: ");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
            
                    case 1: Console.WriteLine("Authentication using user ID and password\n");
                            login();

                             if(validCredential == true)
                            {
                                 transaction();
                            }
                            else
                            {
                               Console.WriteLine(" The entered credentials are incorrect. Transaction failed ");
                               System.Environment.Exit(1);
                            }    
                            break;
 
                    case 2: Console.WriteLine("Authenticate using OTP on registered mobile number");
                            OtpPay();
                            otpValidation();
                            break;
                    case 3: System.Environment.Exit(1);
                            break;

                    default: Console.WriteLine("Enter a valid choice");
                             break;
                }
                Console.ReadKey();
            }

            void transaction()
            {
                float payment=00.00f;
                Console.WriteLine(" Enter the amount to be paid ");
                payment=Convert.ToInt32(Console.ReadLine());
                Customer customer=new Customer();

                float amountpaid= customer.payAmount(payment);
                if (amountpaid!=payment)
                {
                    Console.WriteLine("Transaction failed");
                    System.Environment.Exit(1);
                }
                else
                {
                    Console.WriteLine("Transaction complete");
                    System.Environment.Exit(1);
                }    
            }
               
            void login()
            {
                string username, password;
                int ctr = 0;
                Console.Write("\n\nCheck username and password :\n");
                Console.Write("------------------------------------------------------\n");
           
                do
                {
                    Console.Write("Input a username: ");
                    username = Console.ReadLine();
    
                    Console.Write("Input a password: ");
                    password = Console.ReadLine();
                
                    if(username != "abcd" || password != "1234")
                    ctr++;
                    else
                    ctr=1;
                }
                while((username != "abcd" || password != "1234")  && (ctr != 3));
        
                if (ctr == 3)
                {
                validCredential=false;
                Console.Write("\nLogin attempt three or more times. Try later!\n\n");
                }
                else  
                {
                Console.Write("\nThe password entered successfully!\n\n");
                validCredential=true;
                }
            }
        
            
            
            void OtpPay()
            {
                string[] saAllowedCharacters = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};  
                string sRandomOTP = GenerateRandomOTP(8, saAllowedCharacters);        
                
                string GenerateRandomOTP(int iOTPLength,string[] saAllowedCharacters)  
                {  
                    sOTP = String.Empty;  
                    string sTempChars = String.Empty;   
                    Random rand = new Random();  

                    for (int i = 0; i < iOTPLength; i++)  
                    {  
                        int p = rand.Next(0, saAllowedCharacters.Length);  
                        sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];  
                        sOTP += sTempChars;  
                    }  
                    Console.WriteLine(" your otp is "+sOTP); 
                    return sOTP; 
                } 
            }

            void otpValidation()
            { 
                Console.WriteLine("Please enter the otp generated");
                string typedOtp=Console.ReadLine();
                if(typedOtp==sOTP)
                {
                    Console.WriteLine("The entered otp is correct");
                    transaction();
                }
                else
                {
                    Console.WriteLine("The entered otp is incorrect. Please enter the new otp");
                    OtpPay();
                }
            }
        }
    }
}
}


