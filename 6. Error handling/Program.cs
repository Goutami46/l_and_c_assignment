﻿using System;

public class InsufficientFundException : Exception 
{
    public override string Message 
    {
        get
        {
            return "Your account has Insufficient balance \n";
        } 
    }
}

public class ServerConnectionFailedException : Exception 
{
    public override string Message 
    {
        get
        {
            return "Unable to connect to server. Please try again later \n";
        } 
    }
}
private class Account
{
    public int pin = 1234,enteredPin, withdraw;
    public double balance = 1000;
    public bool validPin = false, connectionStatus = true;
}

class Atm
{
    public static void Main(String[] args)
    {
        Atm atm=new Atm();
        int counter = 0;
        do
        {
            Console.WriteLine("Enter the correct pin \n");
			atm.enteredPin=Convert.ToInt32(Console.ReadLine());

            if(atm.enteredPin != atm.pin)
            counter++;
            else
            counter = 1;
        }
        while((atm.enteredPin != 1234) && (counter != 3));

        if(counter == 3)
        {
            atm.validPin = false;
            Console.WriteLine("\nYou have crossed three attempts. Card blocked!\n\n");
            return;
        }
        else  
        {
            Console.WriteLine("\nWelcome to ABC Bank!\n\n");
            atm.validPin = true;
        }

		while(true)
		{
			Console.WriteLine("1.To check balance");
			Console.WriteLine("2.To withdraw money");
			Console.WriteLine("3.Exit");
			Console.WriteLine("Enter your choice");
			int ch = int.Parse(Console.ReadLine());
			switch(ch)
			{
                case 1: connectionServer();
                        break;
						   
                case 2: withdrawal();
                        break;

                case 3: break;

            }

            void connectionServer()
            {
                try
                {
                    if(atm.connectionStatus == false)
                    {
                        throw new ServerConnectionFailedException();
                    }
                    else
                    {
                        Console.WriteLine("The current balance in your account is"+ atm.balance + "\n");
                    }
                }
                catch(ServerConnectionFailedException e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }

            }

            void withdrawal()
            {
                Console.WriteLine("Enter the amount to be withdrawn \n");
                atm.withdraw = Convert.ToInt32(Console.ReadLine());
                try
                {
                    if(atm.withdraw > atm.balance)
                    {
                        throw new InsufficientFundException();
                    }
                    else
                    atm.balance = atm.balance - atm.withdraw;
                    Console.WriteLine("New balance is" + atm.balance);
                }
                catch (InsufficientFundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }                                

    }
}