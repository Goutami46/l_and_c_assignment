Assignment 3: The below program is to guess the correct number between 1 to 100

def guessing_Number(guessedNumber):
    if guessedNumber.isdigit() and 1<= int(guessedNumber) <=100:
        return True
    else:
        return False

def main():
    randomNumber=random.randint(1,100)
    CorrectGuess=False
    guessedNumber=input("Guess a number between 1 and 100:")
    guessAttempt=0
    while not CorrectGuess:
        if not validateNumber(guessedNumber):
            guessedNumber=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            guessAttempt+=1
            guessedNumber=int(guessedNumber)

        if guessedNumber<randomNumber:
            guessedNumber=input("Too low. Guess again")
        elif guessedNumber>randomNumber:
            guessedNumber=input("Too High. Guess again")
        else:
            print("You guessed it in",guessAttempt,"guesses!")
            CorrectGuess=True